# rss-scraper

App periodically scrapes currencies available on ECB site. 
There are two models: Currency and Scrape, with One-To-Many relation.
While Currency consists of currency name and link to rss feed,
the Scrape represents single scraping task. It has foreign key on Currency,
scraped value, and rss datetime from the site.

Endpoint returns list of Currencies with the latest scraped value, sorted by rss datetime.
Using django-celery-beat and add_periodic_scrapes management command,
 We can set up scheduled scraping tasks, that will periodically update scrape database.


List is available on /api/v1/currency/

#Running rss-scraper locally

Install Python3.6 or higher
```bash
sudo apt install python3.6
```

Make sure you've got pip installed.
```bash
sudo apt install python3-pip
```

Clone the repository
```bash
git clone git@bitbucket.org:pkarasek/rss-scraper.git 
```

Install virtualenv
```bash
pip3 install virtualenv
source env/bin/activate
```

Prepare virtual environment
```bash
virtualenv env -p python3
```

Install project dependencies from requirements.txt
```bash
pip install -r requirements.txt
```

Run all necessary migrations
```bash
./manage.py migrate
```

Create superuser for accessing admin panel
```bash
./manage.py createsuperuser
```

Add Currency instances scraped from ECB
```bash
./manage.py get_currencies
```

Add Periodic tasks for scraping Currencies in the database
```bash
./manage.py add_periodic_scrapes --minute 15 --hour 15 --purge True
```
Alternatively, tasks can be added to run every minute
```bash
./manage.py add_periodic_scrapes --purge True
```

Run celery worker and celery beat
```bash
celery -A app beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler
celery -A app worker -B
```

Start server locally
```bash
./manage.py runserver
```

Remember to change SECRET_KEY on server




