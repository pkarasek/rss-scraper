from django.db import models


class Currency(models.Model):

    class Meta:
        verbose_name = 'Currency'
        verbose_name_plural = 'Currencies'

    name = models.CharField(max_length=256)
    rss_feed_url = models.CharField(max_length=256, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)


class Scrape(models.Model):

    class Meta:
        verbose_name = 'Scrape'
        verbose_name_plural = 'Scrapes'

    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name='scrapes')
    scrape_value = models.FloatField(null=True, blank=True)
    rss_datetime = models.DateTimeField(null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} {}'.format(self.currency, self.rss_datetime)
