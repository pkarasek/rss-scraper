from rest_framework import viewsets, generics, permissions
from scraper.serializers import CurrencySerializer
from scraper.models import Currency, Scrape
from django.db.models import OuterRef, Subquery
'IsAuthenticatedOrReadOnly'


class CurrencyViewset(generics.ListAPIView):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        newest = Scrape.objects.filter(currency=OuterRef('id')).order_by('-rss_datetime')
        return Currency.objects.annotate(exchange_rate=Subquery(newest.values('scrape_value')))



