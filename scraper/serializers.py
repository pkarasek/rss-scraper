from rest_framework import serializers
from scraper.models import Currency, Scrape


class CurrencySerializer(serializers.ModelSerializer):
    exchange_rate = serializers.FloatField(read_only=True)

    class Meta:
        model = Currency
        fields = ('name', 'exchange_rate')



