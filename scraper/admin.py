from django.contrib import admin
from scraper.models import Currency, Scrape

admin.site.register(Currency)
admin.site.register(Scrape)