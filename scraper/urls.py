from django.urls import path
from scraper.views import CurrencyViewset

urlpatterns = [
    path('currency/', CurrencyViewset.as_view(), name='currency'),

]