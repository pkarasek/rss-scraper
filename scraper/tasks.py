from __future__ import absolute_import, unicode_literals
from datetime import datetime
from time import mktime
from celery import shared_task
from scraper.models import Currency, Scrape
import feedparser
import pytz


@shared_task(name='scraper.tasks.currency_scraping')
def currency_scraping(currency_name):
    """
    Creates Scrape instances from rss feed, of chosen currency.
    New Scrape will be created only if feed responds with entry,
    that have datetime different from those already in the database
    """
    currency_instance = Currency.objects.get(name=currency_name)
    rss_feed_url = currency_instance.rss_feed_url
    data = feedparser.parse(rss_feed_url)

    for entry in data.get('entries'):
        entry_datetime = pytz.UTC.localize(datetime.fromtimestamp(mktime(entry.updated_parsed)), is_dst=True)
        scrape_instance, is_new = Scrape.objects.get_or_create(rss_datetime=entry_datetime, currency=currency_instance)

        if is_new is True:
            scrape_instance.scrape_value = float(entry.cb_exchangerate.split('\n')[0])
            scrape_instance.save()
