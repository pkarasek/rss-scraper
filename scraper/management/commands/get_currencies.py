import requests
from django.core.management.base import BaseCommand
from bs4 import BeautifulSoup
from scraper.models import Currency
from tqdm import tqdm


class Command(BaseCommand):
    help = 'Creates model instances for currencies on European Central Bank Site'

    def handle(self, *args, **kwargs):
        url = 'https://www.ecb.europa.eu/home/html/rss.en.html'
        r = requests.get(url)
        if r.status_code == 200:
            soup = BeautifulSoup(r.text, 'html.parser')
            ul_list = soup.find_all('ul', class_='zebraList').pop(1)
            links = ul_list.find_all('a', href=True)

            for link in tqdm(links):
                Currency.objects.get_or_create(name=link.text, rss_feed_url='https://www.ecb.europa.eu{}'.format(link['href']))
            return 'Completed, Currencies count: {}'.format(len(links))

        else:
            return 'Site Unavailable, Status Code: {}'.format(r.status_code)
