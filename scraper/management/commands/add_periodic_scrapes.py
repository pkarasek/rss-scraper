from django.core.management.base import BaseCommand
from scraper.models import Currency
from scraper.tasks import currency_scraping
from django_celery_beat.models import PeriodicTask, CrontabSchedule
from tqdm import tqdm
import json


class Command(BaseCommand):
    help = 'Adding periodic scrape tasks'

    def add_arguments(self, parser):
        parser.add_argument('--minute', type=int)
        parser.add_argument('--hour', type=int)
        parser.add_argument('--purge', type=bool)

    def handle(self, *args, **kwargs):
        m = kwargs.get('minute')
        h = kwargs.get('hour')
        purge = kwargs.get('purge')

        currency_instances = Currency.objects.all()
        schedule, status = CrontabSchedule.objects.get_or_create(
            minute=m or '*',
            hour=h or '*',

        )
        if purge:
            PeriodicTask.objects.all().delete()
        for instance in tqdm(currency_instances):
            p = PeriodicTask.objects.create(name='{} Scrape'.format(instance.name),
                                            task='scraper.tasks.currency_scraping',
                                            args=json.dumps([instance.name]),
                                            crontab=schedule)



